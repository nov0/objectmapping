package service;

import domain.Movie;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class OMDBService {

    private static String THE_MATRIX = "http://www.omdbapi.com/?t=the%20matrix";

    public Movie getMovieFormOmdb() {

        JsonNode movieNode;
        Movie movie = null;

        try {
            URL url = new URL(THE_MATRIX);
            url.openConnection().connect();
            ObjectMapper mapper = new ObjectMapper();
            movieNode = mapper.readTree(url);
            movie = mapper.readValue(movieNode.toString(), Movie.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return movie;
    }
}
