import domain.Movie;
import service.OMDBService;

public class ObjectMapperMain {

    public static void main(String[] args) {
        Movie movie = new OMDBService().getMovieFormOmdb();
        if (movie != null) {
            System.out.println("Movie found:\n" + movie);
        } else {
            System.out.println("Movie not found on OMDB!");
        }
    }

}
